require 'test_helper'

class UserTest < ActiveSupport::TestCase

  should belong_to :organization
  should have_many(:operations).dependent(:destroy)
  should belong_to :employee
  should have_many(:user_licenses).dependent(:destroy)
  should have_many(:licenses).through(:user_licenses)
  should have_many(:attendees).dependent(:destroy)
  should have_many(:events).through(:attendees)

  should validate_presence_of :username

  User::ROLES.each do |role|
    should allow_value(role).for(:role)
  end

  should "not raise exception when username is invalid" do
    user_1 = User.new :username => "without organization"
    user_2 = User.new :username => "|john tardy|"
    assert_nothing_raised Exception do
      user_1.valid?
      user_2.valid?
    end
  end

  test_data do
    FactoryGirl.create(:organization, :contact_support => "michal@taptera.com")
    FactoryGirl.create(:application, :name => "Events", :product => "events")
    FactoryGirl.create(:application, :name => "Rooms",  :product => "rooms")
  end

  setup do
    @organization = Organization.last
    @events = Application.find_by_name "Events"
    @rooms  = Application.find_by_name "Rooms"
  end

  context "licenses" do
    should "create admin user - should have the same licenses as his organization " do
      license = FactoryGirl.create(:license, :application => @events)
      organization = FactoryGirl.create(:organization, :license_ids => [license.id])
      admin = FactoryGirl.create(:admin, :organization => organization)
      assert_equal organization.licenses.enabled.count, admin.licenses.count
    end

    should "update user role from regular to admin - admin has the same licenses as his organization " do
      license = FactoryGirl.create(:license, :application => @events)
      organization =FactoryGirl.create(:organization, :license_ids => [license.id])
      user = FactoryGirl.create(:user, :organization => organization)
      user.update_attribute(:role, 'admin')
      assert_equal organization.licenses.enabled.count, user.licenses.count
    end
  end

  context "given organization" do
    context "validations" do
      should "not allow to change username" do
        user = FactoryGirl.create(:user, :organization => @organization)
        user.username = user.username + "changed"
        user.valid?
        assert_equal([:global, "Username can't be modified"], user.errors.first)
      end

      should "validate format of username (valid)" do
        user = FactoryGirl.create(:user, :organization => @organization, :username => "user@domain.eu")
        assert user.valid?
      end

      should "validate format of username (invalid)" do
        user = FactoryGirl.build(:user, :organization => @organization, :username => "user@domain")
        user.valid?
        assert_equal([:username, "is invalid"], user.errors.first)
      end

      should "allow only one super system user" do
        FactoryGirl.create(:super_system_user)
        user_2 = FactoryGirl.build(:user, :email => "user2@domain.eu", :username => "user2@domain.eu", :role => 'super_system', :organization => @organization)
        user_2.valid?
        assert_equal([:global, "There is already super system user"], user_2.errors.first)
      end

      should "not allow to destroy super_system user" do
        super_system_user = FactoryGirl.create(:super_system_user)
        super_system_user.destroy
        assert_equal([:global, "Super_system users can't be deleted"], super_system_user.errors.first)
      end

      should "not allow to destroy last taptera_admin user" do
        employee = FactoryGirl.create(:employee, :person_id => "e1", :organization => @organization)
        taptera_admin_user_not_last = FactoryGirl.create(:taptera_admin)
        taptera_admin_user_last = FactoryGirl.build(:user, :email => "user2@domain.eu", :username => "user2@domain.eu", :role => 'taptera_admin', :organization => @organization)
        taptera_admin_user_last.employee = employee
        taptera_admin_user_last.save!
        taptera_admin_user_not_last.destroy
        assert_empty(taptera_admin_user_not_last.errors)
        taptera_admin_user_last.destroy
        assert_equal([:global, "Last taptera_admin user can't be deleted"], taptera_admin_user_last.errors.first)
      end

      should "validate email uniqueness in one organization" do
        organization = FactoryGirl.create(:organization, :name => "org2")

        assert_difference('User.count', 2) do
          FactoryGirl.create(:user, :email => "same@domain.eu",
            :organization => @organization, :username => "same@domain.eu"
          )
          FactoryGirl.create(:user, :email => "same@domain.eu",
            :organization => organization
          )
        end

        user2_org1 = FactoryGirl.build(:user, :email => "same@domain.eu",
          :organization => @organization, :username => "same@domain.eu"
        )
        user2_org1.valid?
        assert_equal([:username, "has already been taken"], user2_org1.errors.first)
      end

      ['password', 'pass', 'pas123', '12345678'].each do |password|
        should "validate format of password: '#{password}' (should be invalid)" do
          user = FactoryGirl.build(:user, :organization => @organization, :password => password)
          assert ! user.valid?
        end
      end

      ['PaSSwOrD1234', '!@#1234$%^', '12abcd34__', 'pa_@#%^_', 'PaSSwOrD'].each do |password|
        should "validate format of password: '#{password}' (should be valid)" do
          user = FactoryGirl.build(:user, :organization => @organization, :password => password)
          assert user.valid?
        end
      end

      should "User - create without password" do
        user = FactoryGirl.build(:user, :username => 'm@taptera.com', :email => 'm@taptera.com')
        assert user.valid?
      end

      context "given domain" do
        setup do
          FactoryGirl.create(:domain, :url => "taptera.com", :organization => @organization)
        end
        should "User is invalid when the email is not in domains" do
          user = FactoryGirl.build(:user, :username => "user@different.domain", :organization => @organization)
          assert ! user.valid?
          assert_equal user.errors.full_messages, ["Username  is not in the organization domain"]
        end

        should "User is valid when the email is in domain lists" do
          user = FactoryGirl.build(:user, :organization => @organization)
          user.username = "user@taptera.com"
          assert user.valid?
        end

        context "given more domains" do
          setup do
            FactoryGirl.create(:domain, organization: @organization, url: 'codequest.eu')
            FactoryGirl.create(:domain, organization: @organization, url: 'somecompany.co.uk')
          end
          should "validate domain" do
            assert FactoryGirl.build(:user, username: 'test@taptera.com', organization: @organization).valid?
            assert FactoryGirl.build(:user, username: 'kuba@codequest.eu', organization: @organization).valid?
            assert FactoryGirl.build(:user, username: 'kuba@somecompany.co.uk', organization: @organization).valid?
            assert FactoryGirl.build(:user, username: 'test@welove.taptera.com', organization: @organization).valid?
            assert FactoryGirl.build(:user, username: 'test@manydots.somecompany.co.uk', organization: @organization).valid?
            assert ! FactoryGirl.build(:user, username: 'test@tapteraaa.com', organization: @organization).valid?
            assert ! FactoryGirl.build(:user, username: 'test@abc.tapteraaa.com', organization: @organization).valid?
            assert ! FactoryGirl.build(:user, username: 'test@other.co.uk', organization: @organization).valid?
          end
        end
      end
    end

    context "imports" do
      context "given admin user" do
        setup do
          @admin = FactoryGirl.create(:admin, :organization => @organization)
          @admin.confirm!
        end

        should "import users from csv - synchronous" do
          assert_difference 'ActionMailer::Base.deliveries.size' do
            assert_difference("User.count", 3) do
              User.process_csv(
                resource("test/files/users.csv"),
                @admin.id, @organization.id, Organization, @organization.id
              )
            end
          end

          notification_email = ActionMailer::Base.deliveries.last
          assert_equal "Users import completed", notification_email.subject
          assert_match /Users has been successfully imported!/, notification_email.body.to_s
        end

        context "given licenses" do
          setup do
            colleagues_application = FactoryGirl.create(:application,
              :name    => "Colleagues",
              :product => "colleagues")

            FactoryGirl.create(:license,
              :organization => @organization,
              :application  => @events,
              :enabled => false)

            FactoryGirl.create(:license,
              :organization => @organization,
              :application  => colleagues_application,
              :enabled => true)
          end

          should "not import disabled organization licenses" do
            csv_file = File.new("test/files/users.csv")

            assert_difference("User.count", 3) do
              User.process_csv_file(csv_file, @organization)
            end

            assert_equal 1, User.find_by_email("j.smith1@demo.taptera.com").
              licenses.count
            assert_equal 1, User.find_by_email("a.jones2@demo.taptera.com").
              licenses.count
            assert_equal 1, User.find_by_email("b.white3@demo.taptera.com").
              licenses.count
          end

          should "import users from csve" do
            User.import_csv(
              S3Direct::Resource.new(
                "taptera.console.test", "test/files/user_email_uppercase.csv"
              ),
              @admin, @organization, @organization
            )

            assert_difference "ActionMailer::Base.deliveries.size" do
              assert_difference "User.count", 1 do
                Delayed::Worker.new.work_off
              end
            end

            imported_user = User.find_by_email('mak.atwal@jdsu.com')
            notification_email = ActionMailer::Base.deliveries.last
            assert_not_nil imported_user
            assert_equal "Users import completed", notification_email.subject
            assert_match /Users has been successfully imported!/, notification_email.body.to_s
            assert_equal 1, imported_user.licenses.count
            assert imported_user.licenses.map { |l| l.application.name }.include? "Colleagues"
          end
        end
      end
    end

    context "employee associations" do
      should "associate employee from the same organization as user" do
        organization_2 = FactoryGirl.create(:organization, :name => "second")
        employee = FactoryGirl.create(:employee, :person_id => "e1", :organization => @organization)
        FactoryGirl.create(:employee, :person_id => "e1", :organization => organization_2)
        user = FactoryGirl.create(:user, :organization => @organization, :employee => employee)
        assert_equal('e1', user.employee.person_id)
        employee.delete
        user.reload
        assert_nil(user.employee)
      end
    end

    context "notifications & confirmations" do
      should "User - after change email should delivery error be false" do
        user = FactoryGirl.create(:user, :organization => @organization, :delivery_error => true)
        user.email = 'john@heroku.com'
        user.save!
        user.reload
        assert_equal false, user.delivery_error
      end

      should "notify contact support when user is locked" do
        user = FactoryGirl.create(:user, :organization => @organization)
        admin = FactoryGirl.create(:admin, :organization => @organization)

        ActionMailer::Base.deliveries = []

        assert_difference "ActionMailer::Base.deliveries.size", 1 do
          user.send(:lock_access!)
        end

        assert_equal ["michal@taptera.com"], ActionMailer::Base.deliveries.last.to
        assert_not_nil user.locked_at
      end

      context "given demo license" do
        setup do
          FactoryGirl.create(:license, :organization => @organization,
            :application => @events, :license_type => "Demo")
        end

        should "not send conf email to admin" do
          assert_no_difference "ActionMailer::Base.deliveries.size" do
            FactoryGirl.create(:admin, :organization => @organization)
          end
        end

        should "not send conf email to regular user no matter if there is only Demo like license in user's organization" do
          assert_no_difference 'ActionMailer::Base.deliveries.size' do
            FactoryGirl.create(:user, :organization => @organization)
          end
        end

        should "NOT auto confirm regular user no matter if there is only demo lisense" do
          user_to_check = FactoryGirl.create(:user, :organization => @organization)
          assert ! user_to_check.confirmed?
        end

        should "NOT auto confirm regular user created from attendee no matter what licenses does organization have 2" do
          user_to_check = FactoryGirl.create(:user, :organization => @organization, :is_attendee => true)
          assert ! user_to_check.confirmed?
        end
      end

      context "given evaluation license" do
        setup do
          FactoryGirl.create(:license, :organization => @organization,
            :application => @events, :license_type => "Evaluation")
        end
        should "not send conf email to regular user no matter if there is only Evaluation like license in user's organization" do
          assert_no_difference 'ActionMailer::Base.deliveries.size' do
            FactoryGirl.create(:user, :organization => @organization)
          end
        end

        should "not send conf email to regular user no matter if there are Evaluation and Demo like licenses in user's organization" do
          FactoryGirl.create(:license, :organization => @organization,
            :application => @rooms, :license_type => "Demo")

          assert_no_difference 'ActionMailer::Base.deliveries.size' do
            FactoryGirl.create(:user, :organization => @organization)
          end
        end
      end

      context "given enterprise license" do
        setup do
          FactoryGirl.create(:license, :organization => @organization,
            :application => @events, :license_type => "Enterprise")
        end

        should "not send conf email to regular user no matter if there is Enterprise like license in user's organization" do
          assert_no_difference 'ActionMailer::Base.deliveries.size' do
            FactoryGirl.create(:user, :organization => @organization)
          end
        end

        context "given additional demo license" do
          setup do
           FactoryGirl.create(:license, :organization => @organization,
             :application => @rooms, :license_type => "Demo")
          end

          should "send conf email to regular user no matter if there are Enterprise and Demo like licenses in user's organization" do
            assert_no_difference 'ActionMailer::Base.deliveries.size' do
              FactoryGirl.create(:user, :organization => @organization)
            end
          end

          should "NOT auto confirm admin" do
            user_to_check = FactoryGirl.create(:admin, :organization => @organization)
            assert ! user_to_check.confirmed?
          end

          should "NOT auto confirm taptera_admin" do
            user_to_check = FactoryGirl.create(:taptera_admin)
            assert ! user_to_check.confirmed?
          end

          should "NOT auto confirm regular user created from attendee no matter what licenses does organization have" do
            user_to_check = FactoryGirl.create(:user, :organization => @organization, :is_attendee => true)
            assert ! user_to_check.confirmed?
          end
        end
      end
    end

    context "bounced e-mail processing" do
      should "User get all bounced emails" do
        file = File.open("test/files/bounced_json_response.json", "r")
        contents = file.read
        FakeWeb.register_uri(:get, %r|sendgrid|, :body => contents)
        user = FactoryGirl.create(
          :user, :organization => @organization, :email => "user@testorg.com"
        )
        results = Taptera::Sendgrid.get_bounced_emails(@organization)
        user.reload
        assert_equal true, user.delivery_error
        assert_equal "user@testorg.com", results[0]["email"]
        assert_equal "Unable to resolve MX host system.taptera.com", results[0]["reason"]
      end

      should "User process bounced emails" do
        file = File.open("test/files/bounced_json_response.json", "r")
        contents = file.read
        FakeWeb.register_uri(:get, %r|sendgrid|, :body => contents)

        user = FactoryGirl.create(
          :user, :organization => @organization, :email => "user@testorg.com"
        )
        User.process_bounced_emails(user, @organization)
        user.reload
        assert_equal true, user.delivery_error
      end
    end

    context "field manipulations" do
      should "change employee_person_id to lowercase" do
        user = FactoryGirl.create(:user, :organization => @organization)
        user.employee_person_id = "AAA"
        user.save
        assert_equal "aaa", user.employee_person_id
      end

      should "not change employee_person_id to lowercase -Fixnum" do
        user = FactoryGirl.create(:user, :organization => @organization)
        user.employee_person_id = 1
        user.save
        assert_equal 1, user.employee_person_id
      end

      should "generate confirmation token when user is attendee-like user" do
        user = FactoryGirl.create(:user, :organization => @organization, :is_attendee => true)
        assert_present user.confirmation_token
      end
    end

    context ".deactivate!" do
      should "deactivate a user" do
        user = FactoryGirl.create(:user, :organization => @organization)
        user.confirm!
        user.deactivate!
        user.reload
        assert !user.confirmed?
      end
    end
  end
end