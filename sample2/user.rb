require "csv"
require "open-uri"
require "models/user_social_profile"
require "models/user_import"
require "models/sendgrid"
require "models/user_callbacks"
require "models/user_associations"
require "models/user_validations"
require "models/user_scopes"
require "models/user_snippets_for_email"

class User < ActiveRecord::Base
  extend UserImport

  ROLES = ['regular', 'admin', 'taptera_admin', 'super_system', 'integration', 'system']

  PASSWORD_REGEXP = /\A(((?=.*\d)(?=.*[a-z]))|((?=.*\d)(?=.*[A-Z]))|((?=.*\d)(?=.*[@#\$%^&+=?!"]))|((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[@#\$%^&+=?!"]))|((?=.*[A-Z])(?=.*[@#\$%^&+=?!"]))).*\z/

  devise :database_authenticatable, :recoverable, :confirmable, :lockable, :rememberable, :trackable

  include UserSocialProfile, UserCallbacks, UserAssociations, UserValidations, UserScopes, UserSnippetsForEmail

  validates_uniqueness_of :email, :case_sensitive => (case_insensitive_keys != false), :allow_blank => true, :if => :email_changed?, :scope => :organization_id

  audited :allow_mass_assignment => true, :associated_with => :organization, :except => :encrypted_password
  default_ordering :username, :asc

  ## replace some devise callbacks simulating newer version of devise, we should update devise but this is not that easy
  skip_callback :create, :after, :send_confirmation_instructions
  after_create :send_on_create_confirmation_instructions, :if => :confirmation_required?

  attr_accessible :should_unlock, :email, :password, :password_confirmation, :current_password, :role, :username, :employee_id, :license_ids, :employee_person_id, :shared_token, :inactive, :delivery_error, :photo_url, :confirmation_resend_counter

  attr_accessor :should_unlock

  default_scope :include => :organization

  attr_accessor :is_attendee

  def password_required?
    !password.blank? || !password_confirmation.blank?
  end

  [:taptera_admin, :admin, :super_system, :regular, :integration].each do |role|
    define_method "#{role}?" do
      self.role == "#{role}"
    end
  end

  def system?
    false
  end

  def attendee?
    self.is_attendee || !attendees.empty?
  end

  def api?
    admin? || regular? || super_system? || integration?
  end

  def person_id
    self.employee_person_id
  end

  def active_for_authentication?
    super and !self.inactive
  end

  def confirmation_email_resend_allowed?
    ['admin', 'taptera_admin', 'regular'].include?(self.role)
  end

  def attempt_set_password(params)
    p = { }
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end

  def has_no_password?
    self.encrypted_password.blank?
  end

  def only_if_unconfirmed
    unless_confirmed { yield }
  end

  def update_system_user_licenses(enabled_licenses = nil)
    if self.admin?
      self.licenses = enabled_licenses || self.organization.licenses.enabled
    end
  end

  def unlocking
    if (self.should_unlock && self.access_locked?) || self.encrypted_password != self.encrypted_password_was
      self.locked_at = nil
      self.failed_attempts = 0
    end
  end

  def changed_delivery_error
    self.delivery_error = false if self.email_changed?
    return true
  end

  # overide form devise
  def send_on_create_confirmation_instructions
    # do nothing
  end

  # overwrite from Devise
  def send_confirmation_instructions
    self.regenerate_confirmation_token!
    if self.taptera_admin?
      self.confirmation_resend_counter += 1
      save
      self.devise_mailer.confirmation_instructions(self).deliver

    elsif self.admin?
      self.confirmation_resend_counter += 1
      save
      Mailer.admin_confirmation_instructions(self).deliver

    elsif self.regular? && !self.attendee?
      self.confirmation_resend_counter += 1
      save
      Mailer.user_confirmation_instructions(self).deliver
    end
  end

  def skip_confirmation_for_some_users
    if self.integration? || self.super_system?
      self.confirm!
    end
  end

  def self.process_bounced_emails(user, organization)
    failed_users = Taptera::Sendgrid.get_bounced_emails(organization)
    Mailer.send("bounced_emails", user, organization, failed_users).deliver unless failed_users.empty?
  end

  # make public for attendee
  def regenerate_reset_password_token!
    self.generate_reset_password_token!
  end

  # make public for attendee
  def regenerate_confirmation_token!
    self.generate_confirmation_token! if self.confirmation_token.nil?
  end

  def self.send_confirmation_email_to_all_users(current_user, organization)
    failed_users = []
    organization.users.select { |u| !u.confirmed? }.each do |user|
      begin
        user.send_confirmation_instructions
      rescue
        failed_users << user
      end
    end
    Mailer.user_mass_confirmation_instructions_completed(current_user, organization, failed_users).deliver
  end

  def deactivate!
    return false if self.admin? || self.integration?

    self.confirmation_token = nil
    self.confirmed_at = nil
    save!
  end

  def check_taptera_admin_user
    if self.taptera_admin? and User.taptera_admin_role.count - 1 == 0
      self.errors[:global] << "Last taptera_admin user can't be deleted"
      return false
    end
    true
  end

  def self.create_from_employee(employee, license_ids)
    user = User.new(
      :role               => "regular",
      :email              => employee.email,
      :username           => employee.email,
      :employee_person_id => employee.person_id,
      :license_ids        => license_ids
    )

    user.organization = employee.organization
    user.save
  end


  protected


  alias_method :devise_lock_access!, :lock_access!

  def lock_access!
    self.devise_lock_access!
    email = self.organization.contact_support
    Mailer.user_locked_out(self, email).deliver if email != "support@taptera.com"
  end


  private

  def system_user_uniqueness
    super_system_user_uniqueness
    integration_system_user_uniqueness
  end

  def super_system_user_uniqueness
    if self.super_system?
      self.errors[:global] << "There is already super system user" if User.super_system_role.present? && self.role_was != 'super_system'
    else
      self.errors[:global] << "You are not allowed to change role for super system user" if self.role_was == 'super_system'
    end
  end

  def integration_system_user_uniqueness
    if self.integration?
      self.errors[:global] << "Integration user must have an organization associated" unless self.organization
      self.errors[:global] << "There is already integration user for that organization" if self.role_was != 'integration' && self.organization && self.organization.users.integration_role.present?
    else
      self.errors[:global] << "You are not allowed to change role for integration user" if self.role_was == 'integration'
    end
  end

  def username_not_modified
    if self.username_changed? && !self.new_record?
      self.errors[:global] << "Username can't be modified"
    end
  end

  def check_super_system_user
    if self.super_system?
      self.errors[:global] << "Super_system users can't be deleted"
      return false
    end
  end

  def set_temporary_password
    if self.attendee? || self.admin? || self.taptera_admin? || self.super_system? || self.sign_in_count == -1 # devise hack for user import
      self.password = self.password_confirmation = 'password1234'
    end
  end

  def set_default_role
    self.role = 'regular' if self.role.blank?
  end

  def add_taptera_admin_to_taptera_org
    if self.taptera_admin? && self.organization.nil?
      self.organization = Organization.taptera_inc(Configuration.organization_admin_name).first
    end
  end

  def lowercase_employee_person_id
    self.employee_person_id.to_s.downcase! if self.employee_person_id.present? && self.employee_person_id.is_a?(String)
  end
end