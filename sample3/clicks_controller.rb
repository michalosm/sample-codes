module API
  module ClicksController
    def self.included(base)
      resource = base.name.to_s.split("::").last.underscore.split("_").first

      add_filters(base, resource)
      define_create(base, resource)
      define_find(base, resource)
      set_visibility(base, resource)
    end


    private


    def self.add_filters(base, resource)
      base.class_eval do
        before_filter :"find_#{resource}"
        authorize_resource :"#{resource}", :through => :event
      end
    end

    def self.define_create(base, resource)
      base.class_eval do
        define_method :create do
          @attendee = @event.attendees.for_user(@user).first
          if @attendee
            @click = instance_variable_get(:"@#{resource}").clicks.build
            @click.attendee = @attendee

            if @click.save
              @response = { :clicked => 1 }
            else
              @response = { :error => "Problem with adding click to #{resource}" }
            end
          else
            @response = { :error => "Authenticated user is not attending this event" }
          end
        end
      end
    end

    def self.define_find(base, resource)
      base.class_eval do
        define_method :"find_#{resource}" do
          instance_variable_set(:"@#{resource}",
            @event.send(resource.pluralize).find(params[:"#{resource}_id"])
          ) if @event
        end
      end
    end

    def self.set_visibility(base, resource)
      base.class_eval do
        private :"find_#{resource}"
      end
    end
  end
end