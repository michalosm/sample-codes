require "api/clicks_controller"
require "controllers/organization_event_dependent_controller"

class Api::V1::SponsorClicksController < Api::ApiController
  include Common::OrganizationEventDependentController
  include API::ClicksController
end