require "controllers/event_dependent_controller"

module Common
  module OrganizationEventDependentController
    def self.included(base)
      base.class_eval do
        before_filter :find_organization

        include Common::EventDependentController
      end
    end
  end
end