module Common
  module EventDependentController
    def self.included(base)
      base.class_eval do
        if base.name.to_s.split("::").first.underscore == "api"
          before_filter :find_event_for_api
        else
          before_filter :find_event
          before_filter :set_timezone
        end

        authorize_resource :organization
        authorize_resource :event, :through => :organization
      end
    end
  end
end