require "spec_helper"
require "users_import"
require "factory_girl_rails"

describe "import rooms" do

  before :each do
    @organization = FactoryGirl.create(:organization)
    @user = FactoryGirl.create(:user, :organization => @organization)
    @s3 = S3Direct::FileS3.new("aa", "ss")
    @s3.stub(:download_from_s3)
    @s3.stub(:delete)
    @s3.stub(:temp_path).and_return("spec/files/users.csv")
  end

  it "import rooms - file with one incorrect email " do
    lambda {
      UserMock.process_csv_file(@s3, @organization, @user)
    }.should change(User, :count).by(3)

    notification_email = ActionMailer::Base.deliveries.last
    notification_email.body.to_s.should include("aaa")
    notification_email.subject.should == "Users import completed"
  end

  it "send mail about error import - when file is inccorect" do
    @s3.stub(:temp_path).and_return("spec/files/incorrect_users.csv")
    lambda {
      UserMock.process_csv_file(@s3, @organization, @user)
    }.should_not change(User, :count)
    notification_email = ActionMailer::Base.deliveries.last
    notification_email.subject.should == "Users import failed"
  end

  class UserMock
    extend UsersImport
  end

end