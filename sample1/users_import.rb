require 'csv'
require 's3_direct/file_s3'

module UsersImport

  def process_csv_file(resource, organization_id, user_id)

    organization = Organization.find(organization_id)
    user = User.find(user_id)
    begin
      resource.download_from_s3
      csv = CSV.parse(File.new(resource.temp_path), { :headers => true })
      failed = { }
      success = 0

      additional_data = {
        "sign_in_count" => -1,
        "password" => "password1234"
      }

      csv.each do |row|
        user = User.new do |instance|
          row.each do |column, value|
            instance.send("#{column}=", value)
          end

          additional_data.each do |column, value|
            instance.send("#{column}=", value)
          end

          instance.organization = organization
        end

        if user.save
          success +=1
        else
          failed[user.email] =user.errors.messages
        end
      end
      ImportMailer.import_completed(user.email, "Users import completed", success, failed).deliver
    rescue
      ImportMailer.import_failed(user.email, "Users import failed").deliver
    ensure
      resource.delete
    end

  end

end